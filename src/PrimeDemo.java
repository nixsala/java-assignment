import java.util.Scanner;

public class PrimeDemo {
	public void section1() {
		int opt = 0;
		do {
			// using while to enter user input continously

			int i, storVar = 0, a = 0;
			
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter the number : ");// input user input using scanner
			int num = scan.nextInt();
			
			storVar = num / 2; // store the variable as storeVar
			if (num == 0 || num == 1) { // if function starts
				System.out.println(num + " is not prime number"); // define 0 and 1 are not prime numbers
			} else {
				for (i = 2; i <= storVar; i++) { // for loop starts
					if (num % i == 0) {
						System.out.println(num + " is not prime number"); // define another prime numbers using mod
						a = 1;
						break;

					}
				}
				if (a == 0) {
					System.out.println(num + " is prime number");
				}
			}
			System.out.println("do you want to continue: enter '1' to continue or '0' to exit");
			opt = scan.nextInt();
			if (opt == 0) {
				System.out.println("bye!!");

			}
		} while (opt == 1);

	}
}
