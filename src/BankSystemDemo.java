import java.util.Scanner;

public class BankSystemDemo {

	public double accBal = 0.0;

	public void deposit(double depAmount) {
		accBal = accBal + depAmount;
	}

	public void withdraw(double withAmount) {
		accBal = accBal - withAmount;
	}

	String name="";
	int number;
	public void section1() {
		Scanner sc = new Scanner(System.in);
		String op = "";
		
		
		do {

			BankSystemDemo nixsala = new BankSystemDemo();

			double balance = 0;
			boolean c = true;
			while (c == true) {
				System.out.println("********WELCOME!!! to the CEYLON BANKING SYSTEM********");
				System.out.println("");
				System.out.println("Enter User Name :");
				  name = sc.nextLine();
				  
				  boolean u = true;
					while (u== true) {
				 System.out.println("Enter Account number:");
				  number = sc.nextInt();
				  if (number < 0 || number<10000) {
					  System.out.println("*** Your Account number must be in 5 digits , please re-enter");
				  }  else {
						u=false;
					}
				  }
					
							
				System.out.println("Enter The current balance :");
				balance = sc.nextDouble();
				if (balance < 0) {
					System.out.println("*** Beginning Balance must be at least zero , please re-enter");
				} else {
					c = false;

				}
			}
			nixsala.accBal = balance;

			double depNum = 0;
			boolean a = true;
			while (a == true) {
				System.out.println("enter the number of deposits (0- 5):");
				depNum = sc.nextDouble();
				if (depNum < 0 || depNum > 5) {
					System.out.println("*** Invalid number of deposits, please reenter***");
				} else {
					a = false;
				}
			}

			double witNum = 0;
			boolean b = true;
			while (b == true) {
				System.out.println("enter the number of withdrawls (0- 5):");
				witNum = sc.nextDouble();
				if (witNum < 0 || witNum > 5) {
					System.out.println("*** Invalid number of withdrawls, please reenter***");
				} else {
					b = false;
				}
			}
			double depAmount = 0;
			double depositValue = 0;
			

				for (int d = 1; d <= depNum; d = d + 1) {
					do {
					System.out.println("enter the amount of deposit #  " + d + "  : ");
					depAmount = sc.nextDouble();

					depositValue = depositValue + depAmount;
					if (depAmount < 0) {
						System.out.println("invalid deposit amount, deposit amount should be positive");
					}
				

			} while (depAmount < 0);
				}
			double withAmount = 0;
			double withdrawValue = 0;
			
				for (int w = 1; w <= witNum; w = w + 1) {
					do {
						System.out.println("enter the amount of withdrawl # " + w + "  : ");
						withAmount = sc.nextDouble();
						if (withAmount>balance) {
							System.out.println("withdrawl amount exceeds the current amount.......pls RENTER" );
							withAmount = sc.nextDouble();

						
						}
										
					withdrawValue = withdrawValue + withAmount;
						
				
					
					}while (withAmount < 0 );		
				}
				
			double closingBalance = ((balance + depositValue) - withdrawValue);
			System.out.println(" ***HEY   " + name+ " the closing balance  is Rs  " + closingBalance + "***");

			if (closingBalance >= 50000.00) {
				System.out.println("it is time to invest some money");
			} else {
				if (closingBalance >= 15000.00 && closingBalance <= 49999.99) {
					System.out.println("you should consider the CD");
				} else {
					if (closingBalance >= 1000.00 && closingBalance <= 14999.99) {
						System.out.println("keep up the good work");
					} else {
						if (closingBalance >= 0.00 && closingBalance <= 999.99) {
							System.out.println("your balance is getting low");
						}
					}

				}
			}
			System.out.println("DO YOU WANT TO CONTINUE YES = press y  or  NO = press n");
			op = sc.next();
			if (op.equals("n")) {
				System.out.println("BYE BYE");
				System.exit(0);
			}

		} while (op.equals("y"));
	}
}
